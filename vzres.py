#!/bin/env python3

# Copyright 2022 Daemn
# Licensed under the Apache License, Version 2.0

from argparse import ArgumentParser
import json
import os
from typing import Union, Iterable, Optional
__doc__ = """Display a list of containers and resources allocated to them."""


class AppConfig:
    """App configuration."""

    # All allowed resources.
    allowed_resources = {
        'id': 'Container UUID.',
        'type': 'Type: CT, VM.',
        'state': 'State: stopped, running, etc.',
        'name': 'Container name.',
        'hostname': 'Hostname.',
        'ip': 'IP address',
        'cpu': 'Allocated CPUs.',
        'mem': 'Allocated memory.',
        'uptime': 'Uptime. Only for CT.',
        'home': 'Home directory.',
        'autostart': 'Autostart mode.',
        'autostop': 'Autostop mode.',
        'boot-order': 'Boot order.',
        'dns': 'DNS servers.',
        'search-domains': 'Search domain.',
    }
    # Display a list of allowed resource names.
    list_allowed_resources = False
    # A list of displayed resources.
    out_resources = ['type', 'state', 'name', 'ip']
    # Resource summary.
    show_total = False
    # Display info about all containers.
    show_all_containers = False
    # Hide column titles.
    hide_column_titles = False

    def parse_exec_args(self) -> None:
        parser = ArgumentParser()
        parser.description = __doc__
        parser.add_argument(
            '-l', '--list-allowed-resources',
            dest='list_allowed_resources',
            action='store_true',
            default=False,
            help='List allowed resources.')
        parser.add_argument(
            '-o', '--out-resources',
            dest='out_resources',
            type=str,
            default=','.join(self.out_resources),
            metavar='LIST',
            help='Out resources from LIST. Default: "%(default)s".')
        parser.add_argument(
            '-t', '--total',
            dest='show_total',
            action='store_true',
            default=False,
            help='Show total resources table.')
        parser.add_argument(
            '-a', '--all',
            dest='show_all_containers',
            action='store_true',
            default=False,
            help='Show all containers, not just running ones.')
        parser.add_argument(
            '-H', '--hide-titles',
            dest='hide_column_titles',
            action='store_true',
            default=False,
            help='Hide column titles.')
        parseargs = parser.parse_args()

        self.list_allowed_resources = parseargs.list_allowed_resources
        if self.list_allowed_resources:
            longest_resource: int = max(map(len, self.allowed_resources))
            for resource, descr in self.allowed_resources.items():
                print('{0:>{2}} - {1}'.format(resource, descr, longest_resource))
            parser.exit(status=0)

        self.out_resources = parseargs.out_resources.lower().split(',')
        self.out_resources = list(filter(lambda a: a, self.out_resources))
        if not self.out_resources:
            parser.error('Resources out list is empty.')
        unknown_resources = set(self.out_resources) - set(self.allowed_resources)
        if unknown_resources:
            parser.error(','.join(unknown_resources) + ' is unknown resource.')

        self.show_total = parseargs.show_total

        self.show_all_containers = parseargs.show_all_containers

        self.hide_column_titles = parseargs.hide_column_titles


class Container:
    """Container resource information assembler."""

    uuid: str

    def __init__(self, uuid: str) -> None:
        self.uuid = uuid
        self._info: dict = {}
        self._prlctl_data: Optional[dict] = None

    def __getitem__(self, item):
        if self._prlctl_data is None:
            self._prlctl_data = self._collect_prlctl_data()
        if item not in self._info:
            self._info[item]: Union[None, str, int, list] = self._get_info(item)
        return self._info[item]

    def _collect_prlctl_data(self) -> dict:
        stream = os.popen(f'prlctl list --full -i --json {self.uuid}')
        response_data = json.loads(stream.read())
        if not isinstance(response_data[0], dict):
            raise RuntimeError('Container not found.')
        return response_data[0]

    def _get_info(self, item: str) -> Union[None, str, int, list]:
        try:
            if item == 'ip':
                ips = []
                for hwname, hwinfo in self._prlctl_data['Hardware'].items():
                    if 'ips' in hwinfo:
                        ips += hwinfo['ips'].split()
                ips = map(lambda a: a[:a.find('/')], ips)
                return ','.join(ips)

            if item == 'cpu':
                return self._prlctl_data['Hardware']['cpu']['cpus']

            if item == 'mem':
                memsize: str = self._prlctl_data['Hardware']['memory']['size'].strip()
                multiplier: int = 1024 ** ['Mb', 'Gb', 'Tb'].index(memsize[-2:])
                return int(memsize[:-2]) * multiplier

            if item == 'uptime':
                return readable_time_period(int(self._prlctl_data['Uptime']))

            prlctl_key: str = {
                'id': 'ID',
                'type': 'Type',
                'state': 'State',
                'name': 'Name',
                'hostname': 'Hostname',
                'home': 'Home',
                'autostart': 'Autostart',
                'autostop': 'Autostop',
                'boot-order': 'Boot order',
                'dns': 'DNS Servers',
                'search-domains': 'Search Domains',
            }[item]
            return self._prlctl_data.get(prlctl_key, None)

        except (KeyError, ValueError):
            return None


def readable_time_period(seconds: int) -> str:
    """Return time period in human-readable format.

    :param seconds: Time period in seconds.
    """
    if seconds < 120:
        return f'{seconds} seconds'

    minutes: int = seconds // 60
    if minutes < 120:
        return f'{minutes} minutes'

    hours: int = minutes // 60
    if hours < 48:
        return f'{hours} hours'

    days: int = hours // 24
    return f'{days} days'


def get_total_cpu() -> int:
    """Return the quantity of CPUs on the host."""
    stream = os.popen('nproc')  # Am I sure with this? Maybe read /proc/cpuinfo?
    cpus_num = int(stream.read())
    return cpus_num


def get_mem_info() -> dict:
    """Return information about RAM (Mb)"""
    meminfo = {}
    with open('/proc/meminfo', 'r') as meminfo_file:
        for line in meminfo_file:
            meminfo_line: list = line.strip().split()
            if meminfo_line[0] in ('MemTotal:', 'MemFree:', 'MemAvailable:'):
                name: str = meminfo_line[0][3:-1].lower()
                value: int = int(meminfo_line[1])
                suffix: str = meminfo_line[2].lower()
                multiplier: int = 1024 ** ['kb', 'mb', 'gb', 'tb'].index(suffix)
                meminfo[name] = value * multiplier // 1024
        if len(meminfo) < 3:
            raise ValueError('Wrong format of file /proc/meminfo.')
    return meminfo


class OutTable:
    """Displays a table."""

    table: list

    def __init__(self) -> None:
        self.table = []

    def height(self) -> int:
        """Return the height of the table in rows."""
        return len(self.table)

    def width(self) -> int:
        """Return the width of the table in fields."""
        return max(map(len, self.table), default=0)

    def add_line(self, line: Iterable) -> None:
        """Add row to table."""
        line = map(str, line)
        self.table.append(list(line))

    def append(self, line: Iterable) -> None:
        """Append to the last row in the table."""
        line = list(map(str, line))
        try:
            self.table[-1] += line
        except IndexError:
            self.add_line(line)

    def column_width(self, column_number: int) -> int:
        """Return a width of the column in characters."""
        valid_lines = list(filter(lambda a: len(a) > column_number, self.table))
        return max(map(len, [a[column_number] for a in valid_lines]), default=0)

    def print(self) -> None:
        """Display the table on the screen."""
        column_widths = tuple(self.column_width(a) for a in range(self.width()))
        for line in self.table:
            out_line = [
                '{0:>{1}}'.format(field, column_widths[column_number])
                for column_number, field in enumerate(line)
            ]
            print(' '.join(out_line))


def running_container_ids():
    """Return IDs of running containers."""
    stream = os.popen('prlctl list -Ho uuid')
    for uuid in stream.readlines():
        yield uuid


def all_container_ids():
    """Return IDs of all containers."""
    stream = os.popen('prlctl list -aHo uuid')
    for uuid in stream.readlines():
        yield uuid


def main():
    """The program entry point."""

    # Assemble the app configuration.
    app_config = AppConfig()
    app_config.parse_exec_args()

    # Get the list of containers.
    if app_config.show_all_containers:
        ct_list = list((Container(a) for a in all_container_ids()))
    else:
        ct_list = list((Container(a) for a in running_container_ids()))

    # Display resource summary.
    if app_config.show_total:
        total_cpu: int = get_total_cpu()
        meminfo: dict = get_mem_info()
        ct_cpu_allocated: int = sum((a['cpu'] for a in ct_list if a['state'] == 'running'))
        ct_mem_allocated: int = sum((a['mem'] for a in ct_list if a['state'] == 'running'))

        table = OutTable()
        table.add_line(('', 'CPU', 'Memory'))
        table.add_line(('Total:', total_cpu, f'{meminfo["total"]} Mb'))
        table.add_line(('Allocated:', ct_cpu_allocated, f'{ct_mem_allocated} Mb'))
        table.add_line(('Available:', '', f'{meminfo["available"]} Mb'))
        table.add_line(('Free:', '', f'{meminfo["free"]} Mb'))
        table.print()
        print()

    # Display info about containers.
    table = OutTable()
    if not app_config.hide_column_titles:
        table.add_line(app_config.out_resources)
    for ct in ct_list:
        table.add_line([])
        for field in app_config.out_resources:
            if not app_config.show_all_containers and ct['state'] != 'running':
                continue
            if field == 'mem':
                value = f'{ct[field]} Mb'
            else:
                value = ct[field]
            table.append((value,))
    table.print()


if __name__ == '__main__':
    main()
